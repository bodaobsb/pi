﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {

    public List<GameObject> inventoryItem = new List<GameObject>();    
    
    InventorySlot slotItem;
    [SerializeField] GameObject itemStatsPanel;
	// Use this for initialization
	void Awake () {
        /*
        int children = transform.childCount;
        for (int i = 0; i < children; ++i)
        {
            inventoryItem.Add(transform.GetChild(i).gameObject);
        }
        gameObject.SetActive(false);

    */
    }
    private void OnDisable()
    {
        itemStatsPanel.SetActive(false);
    }
    private void OnEnable()
    {
        itemStatsPanel.SetActive(false);
    }
    public void AddItemToIventory( GameObject obj)
    {
        for (int i = 0; i < inventoryItem.Count; i++)
        {
            slotItem = inventoryItem[i].GetComponent<InventorySlot>();

            if (!slotItem.buttom.GetComponent<Image>().enabled)
            {
                UpdateSlot(obj, i);                
                return;

            }
        }
    }

    void UpdateSlot(GameObject obj, int i)
    {

        slotItem = inventoryItem[i].GetComponent<InventorySlot>();
        slotItem.buttom.GetComponent<Image>().enabled = true;
        slotItem.item = obj;
        //slotItem.damage = obj.GetComponent<Item>().damage;
        //slotItem.itemName = obj.GetComponent<Item>().itemName;
        //inventoryItem[i].GetComponentInChildren<Image>().sprite = obj.GetComponent<SpriteRenderer>().sprite;
        slotItem.buttom.GetComponent<Image>().sprite = obj.GetComponent<SpriteRenderer>().sprite;

    }

}
