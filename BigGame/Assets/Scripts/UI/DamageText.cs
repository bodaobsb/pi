﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageText : MonoBehaviour {


    Vector3 pos = new Vector3(0, 1.5f,0);
    Vector3 randomPos = new Vector3(0.5f,0.25f,0);
    [SerializeField]  float timeAlive;
    private void OnEnable()
    {
        CancelInvoke("Desativa");
        transform.localPosition += pos;
        transform.localPosition += new Vector3(Random.Range(-randomPos.x, randomPos.x), Random.Range(-randomPos.y, randomPos.y), 0);
        Invoke("Desativa",timeAlive);

    }

    void Desativa()
    {
        transform.localPosition -= pos;
        gameObject.SetActive(false);
    }

    
}
