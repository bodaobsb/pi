﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

    [System.Flags]
    public enum ItemType
    {
        Weapon = 1,
        Potion = 2,
        Ring = 3,
        Amulet = 4
    }
    public ItemType itemType;
    List<string> atributos = new List<string>();
    public List<string> atributosSelecionados = new List<string>();
    public List<float> valorDosAtributos = new List<float>();

    public GameObject inventory;
    public string itemName,itemRarity;
    public float damage;
    ChangeWeapon weaponList;
    GameObject playerWeapon;
    GameObject itemStats;
    Manager _manager;
    public Inventory _inventory;
    public bool firstTimeSpawning = true;
    int numberOfAtributes;


    //--------------------------------------------------------------------
    // variaveis de atributo
    public float a_damage, a_critDamage, a_critChance, a_lifeSteal, a_movementSpeed, a_regen, a_armor, a_extraExp, a_extraGold, a_bulletCapacity, a_spellDamage;

    private void Start()
    {
        itemStats = GameObject.FindGameObjectWithTag("Manager").GetComponent<Manager>().ItemStats;
        

        _manager = GameObject.FindGameObjectWithTag("Manager").GetComponent<Manager>();
        _inventory = _manager.inventory.GetComponent<Inventory>();
        //GetReferencedObject();


        if (firstTimeSpawning)
        {
            itemRarity = RollRarity();
            Atributes();
            SortAtributes();
            damage = Random.Range(1f, 5f);
            firstTimeSpawning = false;
        }

        
    }

    private void OnMouseOver()
    {
        itemStats.GetComponent<ItemStatsUI>().selectedItem = gameObject;
        itemStats.SetActive(true);
    }

    private void OnMouseDown()
    {

        
        _inventory.AddItemToIventory(gameObject);
        this.gameObject.SetActive(false);
        itemStats.SetActive(false);

    }
    private void OnMouseExit()
    {
        itemStats.SetActive(false);
    }

    string RollRarity()
    {
        // comum 1, raro 2, lendario 3, divino 4
        int rarity = Random.Range(1, 101);

        if (rarity > 30 && rarity <=100)
        {
            numberOfAtributes = 1;
            return "Comum";
        }
        else if (rarity > 15 && rarity <=30)
        {
            numberOfAtributes = 2;
            return "Raro";
        }
        else if (rarity > 5 && rarity <= 15)
        {
            numberOfAtributes = 3;
            return "Lendario";
        }
        else
        {
            numberOfAtributes = 5;
            return "Divino";
        }

    }

    

    void Atributes()
    {
        switch (itemType)
        {
            case ItemType.Weapon:
                atributos.Clear();
                atributos.Add("Damage");
                atributos.Add("Life Steal");
                atributos.Add("Critical Damage");
                atributos.Add("Critical Chance");
                atributos.Add("Armor");
                atributos.Add("Bullet Capacity");
                break;
            case ItemType.Potion:
                break;
            case ItemType.Ring:
                atributos.Clear();
                atributos.Add("Damage");
                atributos.Add("Movement Speed");
                atributos.Add("Bullet Capacity");
                atributos.Add("Spell Damage");
                atributos.Add("Extra Exp");
                atributos.Add("Extra Gold");
                atributos.Add("Regen");
                break;
            case ItemType.Amulet:
                atributos.Clear();
                atributos.Add("Damage");
                atributos.Add("Movement Speed");
                atributos.Add("Critical Damage");
                atributos.Add("Critical Chance");
                atributos.Add("Spell Damage");
                atributos.Add("Extra Exp");
                atributos.Add("Extra Gold");
                atributos.Add("Regen");
                atributos.Add("Life Steal");
                atributos.Add("Armor");
                atributos.Add("Bullet Capacity");

                break;
            default:
                break;
        }
    }

    void SortAtributes()
    {
        atributosSelecionados.Clear();

        for (int i = 0; i < numberOfAtributes; i++)
        {
            
            int a = Random.Range(0, atributos.Count);
            string name = atributos[a];           

            if (!atributosSelecionados.Contains(name))
            {
                atributosSelecionados.Add(name);
                atributos.Remove(name);
            }
            else
            {
                
                print("atributos iguais");
                //SortAtributes();
                
            }

            switch (name)
            {
                case "Damage":
                    a_damage = Damage();
                    valorDosAtributos.Add(a_damage);
                    break;
                case "Movement Speed":
                    a_movementSpeed = MovementSpeed();
                    valorDosAtributos.Add(a_movementSpeed);
                    break;
                case "Critical Damage":
                    a_critDamage = CriticalDamage();
                    valorDosAtributos.Add(a_critDamage);
                    break;
                case "Critical Chance":
                    a_critChance = CriticalChance();
                    valorDosAtributos.Add(a_critChance);
                    break;
                case "Spell Damage":
                    a_spellDamage = SpellDamage();
                    valorDosAtributos.Add(a_spellDamage);
                    break;
                case "Extra Gold":
                    a_extraGold = ExtraGold();
                    valorDosAtributos.Add(a_extraGold);
                    break;
                case "Extra Exp":
                    a_extraExp = ExtraExp();
                    valorDosAtributos.Add(a_extraExp);
                    break;
                case "Regen":
                    a_regen = Regen();
                    valorDosAtributos.Add(a_regen);
                    break;
                case "Life Steal":
                    a_lifeSteal = LifeSteal();
                    valorDosAtributos.Add(a_lifeSteal);
                    break;
                case "Armor":
                    a_armor = Armor();
                    valorDosAtributos.Add(a_armor);
                    break;
                case "Bullet Capacity":
                    a_bulletCapacity = BulletCapacity();
                    valorDosAtributos.Add(a_bulletCapacity);
                    break;


                default:
                    print("nao teve atributos sorteado");
                    break;
            }
        }
        

    }
    

    float Damage()
    {
        float damage;
        switch (itemRarity)
        {
            case "Comum": damage = Random.Range(1f, 5f);

                break;
            case "Raro":
                damage = Random.Range(5f, 10f);
                break;
            case "Lendario":
                damage = Random.Range(10f, 15f);
                break;
            case "Divino":
                damage = Random.Range(15f, 20f);
                break;

            default:
                damage = 0;
                break;
               
        }
        return RoundNumber(damage);
    }

    float MovementSpeed()
    {
        float movementSpeed;
        switch (itemRarity)
        {
            case "Comum":
                movementSpeed = Random.Range(1f, 1.5f);

                break;
            case "Raro":
                movementSpeed = Random.Range(1.5f, 2.75f);
                break;
            case "Lendario":
                movementSpeed = Random.Range(2.75f, 4f);
                break;
            case "Divino":
                movementSpeed = Random.Range(5f, 9f);
                break;

            default:
                movementSpeed = 0;
                break;

        }
        return RoundNumber(movementSpeed);
    }

    float CriticalDamage()
    {
        float criticalDamage;
        switch (itemRarity)
        {
            case "Comum":
                criticalDamage = Random.Range(10f, 15f);

                break;
            case "Raro":
                criticalDamage = Random.Range(15f, 25f);
                break;
            case "Lendario":
                criticalDamage = Random.Range(25f, 45f);
                break;
            case "Divino":
                criticalDamage = Random.Range(45f, 70f);
                break;

            default:
                criticalDamage = 0;
                break;

        }
        return RoundNumber(criticalDamage / 100);
    }

    float CriticalChance()
    {
        float criticalChance;
        switch (itemRarity)
        {
            case "Comum":
                criticalChance = Random.Range(5f, 10f);

                break;
            case "Raro":
                criticalChance = Random.Range(10f, 15f);
                break;
            case "Lendario":
                criticalChance = Random.Range(15f, 20f);
                break;
            case "Divino":
                criticalChance = Random.Range(20f, 25f);
                break;

            default:
                criticalChance = 0;
                break;

        }
        return RoundNumber(criticalChance / 100);
    }

    float SpellDamage()
    {
        float spellDamage;
        switch (itemRarity)
        {
            case "Comum":
                spellDamage = Random.Range(1f, 5f);

                break;
            case "Raro":
                spellDamage = Random.Range(5f, 10f);
                break;
            case "Lendario":
                spellDamage = Random.Range(10f, 15f);
                break;
            case "Divino":
                spellDamage = Random.Range(15f, 20f);
                break;

            default:
                spellDamage = 0;
                break;

        }
        return RoundNumber(spellDamage / 100);
    }

    float ExtraGold()
    {
        float extraGold;
        switch (itemRarity)
        {
            case "Comum":
                extraGold = Random.Range(1f, 2.5f);

                break;
            case "Raro":
                extraGold = Random.Range(2.5f, 5f);
                break;
            case "Lendario":
                extraGold = Random.Range(5f, 7.5f);
                break;
            case "Divino":
                extraGold = Random.Range(7.5f, 10f);
                break;

            default:
                extraGold = 0;
                break;

        }
        return RoundNumber(extraGold / 100);
    }

    float ExtraExp()
    {
        float extraExp;
        switch (itemRarity)
        {
            case "Comum":
                extraExp = Random.Range(1f, 2.5f);

                break;
            case "Raro":
                extraExp = Random.Range(2.5f, 5f);
                break;
            case "Lendario":
                extraExp = Random.Range(5f, 7.5f);
                break;
            case "Divino":
                extraExp = Random.Range(7.5f, 10f);
                break;

            default:
                extraExp = 0;
                break;

        }
        return RoundNumber(extraExp / 100);
    }

    float Regen()
    {
        float regen;
        switch (itemRarity)
        {
            case "Comum":
                regen = Random.Range(0.5f, 2.5f);

                break;
            case "Raro":
                regen = Random.Range(2.5f, 4f);
                break;
            case "Lendario":
                regen = Random.Range(4f, 6f);
                break;
            case "Divino":
                regen = Random.Range(6f, 9f);
                break;

            default:
                regen = 0;
                break;

        }
        return RoundNumber(regen);
    }

    float LifeSteal()
    {
        float lifeSteal;
        switch (itemRarity)
        {
            case "Comum":
                lifeSteal = Random.Range(2f, 5f);

                break;
            case "Raro":
                lifeSteal = Random.Range(5f, 8f);
                break;
            case "Lendario":
                lifeSteal = Random.Range(8f, 12.5f);
                break;
            case "Divino":
                lifeSteal = Random.Range(12.5f, 20f);
                break;

            default:
                lifeSteal = 0;
                break;

        }
        return RoundNumber(lifeSteal/100);
    }

    float Armor()
    {
        float armor;
        switch (itemRarity)
        {
            case "Comum":
                armor = Random.Range(1f, 2.5f);

                break;
            case "Raro":
                armor = Random.Range(2.5f, 5f);
                break;
            case "Lendario":
                armor = Random.Range(5f, 7.5f);
                break;
            case "Divino":
                armor = Random.Range(7.5f, 10f);
                break;

            default:
                armor = 0;
                break;

        }
        return  RoundNumber(armor);
    }

    float BulletCapacity()
    {
        float bulletCapacity;
        switch (itemRarity)
        {
            case "Comum":
                bulletCapacity = Random.Range(20, 51);

                break;
            case "Raro":
                bulletCapacity = Random.Range(50, 81);
                break;
            case "Lendario":
                bulletCapacity = Random.Range(80, 111);
                break;
            case "Divino":
                bulletCapacity = Random.Range(110, 151);
                break;

            default:
                bulletCapacity = 0;
                break;

        }

        return bulletCapacity;
    }

    float RoundNumber(float num)
    {
        num *= 100;
        num = Mathf.RoundToInt(num);
        num /= 100;
        return num;
    }
}
