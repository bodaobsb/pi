﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemStatsUI : MonoBehaviour {

    public List<Text> text = new List<Text>();
    public GameObject selectedItem;
    [SerializeField] Text nameText,typeText;
    Item _item;

    private void OnEnable()
    {
        _item = selectedItem.GetComponent<Item>();
        ClearText();
        nameText.text = _item.itemName + "    " + _item.itemRarity;
        typeText.text = _item.itemType.ToString();
        for (int i = 0; i < _item.atributosSelecionados.Count; i++)
        {
            text[i].text = _item.atributosSelecionados[i] + "      " + _item.valorDosAtributos[i];
        }



    }

    void ClearText()
    {
        for (int i = 0; i < text.Count; i++)
        {
            text[i].text = "";
        }
    }

}
