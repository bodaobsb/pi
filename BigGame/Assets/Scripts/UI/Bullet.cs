﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    [SerializeField] float speed = 10f;
    [SerializeField] float bulletDuration;
    
    public float damage;
    Weapon weapon;
    Player player;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }
    private void OnDisable()
    {
        CancelInvoke("Desativa");
    }

    void Desativa()
    {
        gameObject.SetActive(false);

    }
    void OnEnable()
    {
        Vector3 mousePosition = Input.mousePosition;
        Vector3 distance = mousePosition - transform.position;
        this.GetComponent<Rigidbody>().velocity = transform.forward* speed;
        Invoke("Desativa", bulletDuration);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            Desativa();
            other.GetComponent<EnemyHealth>().TakeDamage(player.RollPlayerDamage());

        }
        if (other.CompareTag("Wall"))
        {
            Desativa();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            //Destroy(this.gameObject, 0.025f);
            Desativa();

        }
    }
}
