﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CharacterSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public GameObject selectedItem;
    //public float damage;
    [SerializeField]ChangeWeapon _changeWeapon;
    [SerializeField]Inventory _inventory;
    Manager _manager;
    void Start()
    {
        _manager = GameObject.FindGameObjectWithTag("Manager").GetComponent<Manager>();
    }

    public void ChangeSprite()
    {

        this.gameObject.GetComponent<Image>().sprite = selectedItem.GetComponent<SpriteRenderer>().sprite;
    }

    public void UpdateCurrentPlayerSlot(GameObject obj)
    {

        CheckSlot();
        
        selectedItem = obj;
        if (selectedItem.GetComponent<Item>().itemType == Item.ItemType.Weapon)
        {
            ClearWeapon();
            foreach (GameObject item in _changeWeapon.armas)
            {
                if (item.name == selectedItem.GetComponent<Item>().itemName)
                {
                    item.SetActive(true);
                    item.GetComponent<Weapon>().damage = selectedItem.GetComponent<Item>().damage;
                }
            }
        }
        
        
    }

    
    void CheckSlot()
    {
        if (!selectedItem)
        {
            return;
        }
        else
        {
          _inventory.AddItemToIventory(selectedItem);

            
        }
    }



    void ClearWeapon()
    {
        for (int i = 0; i < _changeWeapon.armas.Count; i++)
        {
            _changeWeapon.armas[i].SetActive(false);
        }
    }


    public void OnPointerExit(PointerEventData eventData)
    {
        _manager.ItemStats.SetActive(false);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (selectedItem != null)
        {
            _manager.ItemStats.GetComponent<ItemStatsUI>().selectedItem = selectedItem;
            _manager.ItemStats.SetActive(true);

        }
    }

 
}
