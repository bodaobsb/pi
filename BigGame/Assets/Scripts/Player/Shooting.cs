﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour {
    Weapon _weapon;
    float timer;
    [SerializeField] Transform body,weapon;
    Pooling pooling;
    public Transform newPos;
    
	// Use this for initialization

	void Start () {
        pooling = GameObject.FindGameObjectWithTag("Pooling").GetComponent<Pooling>();   
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;

       
    }

    public void Shoot()
    {
        foreach (GameObject armas in weapon.GetComponent<ChangeWeapon>().armas)
        {
            if (armas.activeSelf)
            {
                _weapon = armas.GetComponent<Weapon>();
            }
        }
       // _weapon = weapon.GetComponent<ChangeWeapon>().ar;
        string weaponType = _weapon.weaponType.ToString();
        switch (weaponType)
        {
            case "Normal":
                if (timer >= _weapon.fireRate)
                {
                    for (int y = 0; y < _weapon.bulletStartPos.Length; y++)
                    {
                        GameObject newBullet = _weapon.poolWeapon.GetPooledObject(_weapon.pooledList);
                        //GameObject newMuzzle = pooling.GetPooledMuzzle();

                        if (newBullet != null)
                        {
                            _weapon.bulletShell.Play();
                            newBullet.transform.position = _weapon.bulletStartPos[y].position;
                            newBullet.transform.rotation = newPos.rotation;
                            newBullet.SetActive(true);


                        }
                        /*
                        if (newMuzzle != null)
                        {
                            newMuzzle.transform.position = _weapon.muzzlePosition.position;
                            newMuzzle.transform.rotation = _weapon.muzzlePosition.rotation;
                            newMuzzle.SetActive(true);
                        }
                        */
                        _weapon.currentAmmo--;
                    }
                    GetComponent<Animator>().SetTrigger("Recoil");
                    _weapon._audioSource.Stop();
                    _weapon._audioSource.PlayOneShot(_weapon.shotSoundClip, 0.25f);
                    timer = 0;
                }
                
                break;
            case "Random":
                if (timer >= _weapon.fireRate)
                {
                    //gunAudio.PlayOneShot(weapon.weaponClip, 0.25f);
                    int spawnPointIndex = Random.Range(0, _weapon.bulletStartPos.Length);
                    GameObject newBullet = _weapon.poolWeapon.GetPooledObject(_weapon.pooledList);
                    if (newBullet != null)
                    {
                        newBullet.transform.position = _weapon.bulletStartPos[spawnPointIndex].position;
                        newBullet.transform.rotation = newPos.rotation;
                        newBullet.SetActive(true);

                    }
                    _weapon.currentAmmo--;
                    timer = 0;

                }

                break;


            default:
                return;
        }
    }

   

}
