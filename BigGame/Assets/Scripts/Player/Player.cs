﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float currentGold;
    GameObject weapon;
    CharacterSlot charSlot;
    [HideInInspector]
    public float a_damage, a_critDamage, a_critChance, a_lifeSteal, a_movementSpeed, a_regen, a_armor, a_extraExp, a_extraGold, a_bulletCapacity, a_spellDamage;

    [Header("Experiencia")]
    public int level;
    public float currentExp;
    public float expToLevelUp;
    [Header("Atributos")]
    public bool hasCrit;
    public float currentDamage;
    [SerializeField] GameObject[] slots;

    private void Start()
    {
       
    }

    public void UpdatePlayerStats()
    {
        ResetValues();
        //------------ Update stats with item's SECUNDARY atributes
        for (int i = 0; i < slots.Length; i++)
        {
            charSlot = slots[i].GetComponent<CharacterSlot>();

            if (charSlot.selectedItem != null)
            {
                a_damage += charSlot.selectedItem.GetComponent<Item>().a_damage;
                a_critDamage += charSlot.selectedItem.GetComponent<Item>().a_critDamage;
                a_critChance += charSlot.selectedItem.GetComponent<Item>().a_critChance;
                a_lifeSteal += charSlot.selectedItem.GetComponent<Item>().a_lifeSteal;
                a_movementSpeed += charSlot.selectedItem.GetComponent<Item>().a_movementSpeed;
                a_regen += charSlot.selectedItem.GetComponent<Item>().a_regen;
                a_armor += charSlot.selectedItem.GetComponent<Item>().a_armor;
                a_extraExp += charSlot.selectedItem.GetComponent<Item>().a_extraExp;
                a_extraGold += charSlot.selectedItem.GetComponent<Item>().a_extraGold;
                a_bulletCapacity += charSlot.selectedItem.GetComponent<Item>().a_bulletCapacity;
                a_spellDamage += charSlot.selectedItem.GetComponent<Item>().a_spellDamage;

            }

        }
        //------------ Update stats with item's PRIMARY atributes
        weapon = GameObject.FindGameObjectWithTag("Weapon");
        currentDamage = a_damage + weapon.GetComponent<Weapon>().damage;
        
    }

    public void UpdatePlayerExperience(float amount)
    {
        currentExp += amount;
        if (currentExp >= expToLevelUp)
        {
            level++;
            currentExp = 0;
            if (level <=10)
            {
                expToLevelUp = level * 100;
            }
            else
            {
                expToLevelUp = level * 105;
            }
            
        }
    }

    void ResetValues()
    {
        a_damage = 0;
        a_critDamage = 0;
        a_critChance = 0;
        a_lifeSteal = 0;
        a_movementSpeed = 0;
        a_regen = 0;
        a_armor = 0;
        a_extraExp = 0;
        a_extraGold = 0;
        a_bulletCapacity = 0;
        a_spellDamage = 0;
    }

    public float RollPlayerDamage()
    {
        weapon = GameObject.FindGameObjectWithTag("Weapon");
        if (a_critChance > 0)
        {
            float rollCritChance = Random.Range(0f, 1f);
            if (rollCritChance <= a_critChance)
            {
                print("critou");
                currentDamage = (a_damage + weapon.GetComponent<Weapon>().damage) * (1 + a_critDamage);
                hasCrit = true;

                return RoundNumber(currentDamage);


            }
        }

        currentDamage = a_damage + weapon.GetComponent<Weapon>().damage;
        hasCrit = false;
        return RoundNumber(currentDamage);

    }

    float RoundNumber(float number)
    {
        number *= 100;
        number = Mathf.Round(number);
        number /= 100;
        return number;
    }
}
