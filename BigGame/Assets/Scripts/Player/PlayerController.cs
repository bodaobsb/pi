﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    public static bool IsLeft, IsRight, IsUp, IsDown;
    private float _LastX, _LastY;

    public float baseSpeed = 5;
    public bool player1;
    Shooting _shooting;
    float moveX, moveZ;
    float directionX;
    Vector2 direction;
    [SerializeField] GameObject _inventory;

    public string horizontalMovement, verticalMovement,shoot,inventory;


    public Transform turnObject;
	// Use this for initialization
	void Start () {
        _shooting = GetComponent<Shooting>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        Move();
        // TEST
        if (Input.GetKeyDown(KeyCode.P))
        {
            SceneManager.LoadScene("cena");
        }

        Shoot();
        //RotateMouse();
        /*
        DisplayInventory();

        if (player1)

            
        else

           RotateJoystick();
           */
    }

    private void RotateJoystick()
    {
        if (Input.GetAxis("Horizontal_Rotation") != 0 || Input.GetAxis("Vertical_Rotation") != 0)
        {
            float horizontal = Input.GetAxis("Horizontal_Rotation") * Time.deltaTime;
            float vertical = Input.GetAxis("Vertical_Rotation") * Time.deltaTime;
            float angle = Mathf.Atan2(vertical, horizontal) * Mathf.Rad2Deg;
            turnObject.transform.eulerAngles = new Vector3(0, 0, angle - 90);
        }
    }



    public void Move()
    {
        moveX = Input.GetAxis("Horizontal");
        moveZ = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveX, 0,moveZ);
        GetComponent<Rigidbody>().velocity = movement*(baseSpeed + GetComponent<Player>().a_movementSpeed);
        
    }

   

    void RotateMouse()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = 1;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        directionX = (mousePosition.x - transform.position.x);
        direction = new Vector2(directionX, mousePosition.y - transform.position.y);

        turnObject.forward = direction;
    }




    void Shoot()
    {
        if (Input.GetMouseButton(0))
        {
            _shooting.Shoot();
        }

    }

    void DisplayInventory()
    {
        if (player1)
        {
            if (Input.GetKeyDown(KeyCode.I))
            {
                _inventory.SetActive(!_inventory.activeSelf);

            }
        }
        else
        {
            DPadInputSystem();
        }


    }

    private void DPadInputSystem()
    {
        float x = Input.GetAxis("DPad X");
        float y = Input.GetAxis("DPad Y");

        IsLeft = false;
        IsRight = false;
        IsUp = false;
        IsDown = false;

        if (_LastX != x)
        {
            if (x == -1)
                IsLeft = true;
            else if (x == 1)
                IsRight = true;
        }

        if (_LastY != y)
        {
            if (y == -1)
                IsDown = true;
            else if (y == 1)
                IsUp = true;
        }

        _LastX = x;
        _LastY = y;

        if (PlayerController.IsUp)
        {
            _inventory.SetActive(!_inventory.activeSelf);
        }
    }
}
