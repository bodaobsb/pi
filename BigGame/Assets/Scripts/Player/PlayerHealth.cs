﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour {

    bool damaged;
    bool isDead;
    public int startingHealth;
    public float currentHealth;
    Player _playerAtributes;
    float damageMultiplier;
    void Start()
    {
        _playerAtributes = GetComponent<Player>();
        currentHealth = startingHealth;
    }

    void Update()
    {
        
        if (currentHealth < startingHealth && !isDead)
        {
            currentHealth += (0.5f + _playerAtributes.a_regen) * Time.deltaTime;
        }

        if (damaged)
        {
            // damageImage
        }
        else
        {
            //damageImage
        }
        damaged = false;
    }

    public void TakeDamage(float amout)
    {
        if (currentHealth > 1)
        {
            //anim e etc
        }

        damaged = true;
        damageMultiplier = 1 - (0.05f * _playerAtributes.a_armor / (1 + 0.05f * Mathf.Abs(_playerAtributes.a_armor)));
        currentHealth -= amout * damageMultiplier;

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }


    void Death()
    {
        isDead = true;

    }
}
