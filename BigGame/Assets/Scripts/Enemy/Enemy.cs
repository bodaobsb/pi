﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public int level;
    public int exp;
    public float gold;
    Player player;
    [SerializeField] GameObject goldObject;
    [SerializeField] GameObject[] droppableItems;
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        exp = 10 * level;
    }



    public void DropSystem()
    {
        float chance = Random.Range(0f, 100f);
        float chanceToDropGold = 25 + (level * 0.25f);
        if (chance <= chanceToDropGold)
        {
            DropGold();
        }
        chance = Random.Range(0f, 100f);
        float chanceToDropItem = 15 + (level * 0.5f);
        
        if (chance <= chanceToDropItem)
        {
            DropItem();
        }

    }


    void DropGold()
    {
        float goldToDrop = Random.Range(1, 10f)*level * (1+ player.a_extraGold);
        goldToDrop = Mathf.Round(goldToDrop);
        GameObject goldItem = (GameObject)Instantiate(goldObject, transform.position, transform.rotation);
        goldItem.GetComponent<Gold>().gold = goldToDrop;

    }

    void DropItem()
    {
        int itemToDrop = Random.Range(1, droppableItems.Length);
        Instantiate(droppableItems[itemToDrop], transform.position, transform.rotation);
    }







}
