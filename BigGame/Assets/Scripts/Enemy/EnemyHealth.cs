﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {
    public float currentHealth, startingHealth;
    bool isDead;
    Enemy _enemy;
    GameObject player;
    Pooling pooling;
    private void Start()
    {
        pooling = GameObject.FindGameObjectWithTag("Pooling").GetComponent<Pooling>();
        player = GameObject.FindGameObjectWithTag("Player");
        _enemy = GetComponent<Enemy>();
        currentHealth = startingHealth;
    }
    void Update()
    {

       
    }

    public void TakeDamage(float amout)
    {
        if (currentHealth > 1)
        {
            //DisplayDamageText(amout);
            //anim e etc
        }

        currentHealth -= amout;

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }


    void Death()
    {
        isDead = true;
        // give exp to player
        player.GetComponent<Player>().UpdatePlayerExperience(_enemy.exp);
        GetComponent<Enemy>().DropSystem();
        Destroy(gameObject);
    }
    /*
    private void DisplayDamageText(float amount)
    {
        GameObject newText = pooling.GetPooledDamageText();
        if (newText != null)
        {
            newText.transform.position = transform.position;
            newText.transform.rotation = transform.rotation;

            if (player.GetComponent<Player>().hasCrit)
            {
                newText.GetComponent<TextMesh>().color = Color.yellow;
            }
            else
            {
                newText.GetComponent<TextMesh>().color = Color.gray;
            }
            newText.GetComponent<TextMesh>().text = amount.ToString();
            newText.SetActive(true);
        }
    }
    */
}
